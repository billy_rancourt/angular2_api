import { Component, OnInit } from '@angular/core';
import { BlogService } from './shared/blog.service';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  providers: [ BlogService ]
})
export class AppComponent implements OnInit {
  title = 'app works!';

  users:any[];

  constructor(public blogService : BlogService){}

  ngOnInit(){
    console.log('test');
    this.blogService.getUsers().subscribe((data) => this.users = data);
  }

}
